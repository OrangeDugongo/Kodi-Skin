# Kodi-Skin
Ceautiful collection of skins for kodi

## Features
Big modern homescreen layout, with featured *Most Popular, New Movie, Watchlist and Oscar Winners.* 


![Blue Movie][blue_movie]
TV Show with your *progress.*


![Blue TV Show][blue_tv_show]
Easy, one click, shortcuts management.


![Blue Shorcuts][green_shortcuts]
Support fot kodi v.17 krypton

## Plugin Required

- Skin Titan
- Exodus
- IPTV Simple Client(if you want live tv)
- YouTube(if you want YouTube)
- Weather(if you wanr Weather)
- Addon Installer
- Trakt.tv sync(if you want *Watchlist* and *progress*)


[blue_movie]: image/KODIFLIX_MOVIE_BLUE.png
[blue_tv_show]: image/KODIFLIX_TVSHOW_BLUE.png
[green_shortcuts]: image/KODIFLIX_SHORTCUTS_GREEN.png
